#!/usr/bin/env python

# -----------------------------------------
# main1.py
# creating first flask application
# -----------------------------------------
#
#
from flask import Flask, render_template
from models import app, db, Ride, Accident, Park
#from create_db import create_rides,create_accidents
import shlex, subprocess



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/parks')
def parks():
    parks_db = db.session.query(Park).all()
    return render_template('parks.html',parks_db=parks_db)

@app.route('/parksModel')
def parkData():
    parks_db = db.session.query(Park).all()
    return render_template('parksModel.html',parks_db=parks_db)

@app.route('/rides')
def ride():
    rides_db = db.session.query(Ride).all()
    return render_template('rides.html',rides_db=rides_db)

@app.route('/ridesModel')
def rideData():
    rides_db = db.session.query(Ride).all()
    return render_template('ridesModel.html',rides_db=rides_db)

@app.route('/accidents')
def accident():
    accidents_db = db.session.query(Accident).all()
    return render_template('accidents.html',accidents_db=accidents_db)

@app.route('/accidentsModel')
def accidentData():
    accidents_db = db.session.query(Accident).all()
    return render_template('accidentsModel.html',accidents_db=accidents_db)

@app.route('/searchResults')
def searchResult():
    accidents_db = db.session.query(Accident).all()
    rides_db = db.session.query(Ride).all()
    parks_db = db.session.query(Park).all()
    return render_template('searchResults.html',accidents_db=accidents_db,rides_db=rides_db,parks_db=parks_db)

@app.route('/test/')
def test():
	p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			stdin=subprocess.PIPE)
	out, err = p.communicate()
	output=err+out
	output = output.decode("utf-8") #convert from byte type to string type

	return render_template('test.html', output = "<br/>".join(output.split("\n")))

# create a flask object (flask needs an object to represent the application)


if __name__ == "__main__":
    app.run(debug = True)
# ----------------------------------------
# end of main1.py
# -----------------------------------------
