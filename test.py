import os
import sys
import unittest
from models import db, Accident,Park, Ride

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        s = Accident(id='20',category='accidents',code='a20',ext_link="https://en.wikipedia.org/wiki/Peter_Pan",on_ride="Ferris Wheel",location="Pirate Ship in Neverland, NV",park="Pirate Ship",city="Neverland",state="NV",cause="Broken Plank",dmg="Fall",hurt="1",fatal="no",year="1850",date_of_event="7/7/1850",description="A pirate walking the plank fell into stormy seas when the plank suddenly snapped. He grabbed onto driftwood and survived.")
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Accident).filter_by(id = '20').one()
        self.assertEqual(str(r.id), '20')
        db.session.query(Accident).filter_by(id = '20').delete()
        db.session.commit()

    def test_source_insert_2(self):
        s = Ride(id='21',category='rides',code='LEO',name="Low Earth Orbit Space Coaster",max_height="522720 ft",speed="out of this world",duration="60 minutes",featured_in="Tesla-SpaceX Interplanetary Fun Park",had_fatal="yes",first_built="2035",history="Elon Musk's 'Amusement for Humans' conglomerate premiered the first space rides in 2035.",description="Get the full Astronaut Experience on this crazy coaster!")
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Ride).filter_by(id = '21').one()
        self.assertEqual(str(r.id), '21')
        db.session.query(Ride).filter_by(id = '21').delete()
        db.session.commit()

    def test_source_insert_3(self):
        s = Park(id='22',category='parks',code='notDisney',name="Totally Not DisneyLand",location="Probably California or Florida, USA",city="Probably California or Florida",state="USA",size="500 acres",opened="2018",age="1",season="24/7/365",annual_visitors="20 million",ticket_price="$20 per ride",has_fw="of course",has_rc="are you kidding?",has_sr="absolutely",had_fatal="nope! very safe!",description="Any common attributes between this park and the parks of the Walt Disney Company(TM) such as DisneyLand(TM) are pure coincidence, we promise.")
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Park).filter_by(id = '22').one()
        self.assertEqual(str(r.id), '22')
        db.session.query(Ride).filter_by(id = '22').delete()
        db.session.commit()

if __name__ == '__main__':
    unittest.main()
