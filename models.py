from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__, template_folder='template')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:password@localhost:5432/themeparks')
db = SQLAlchemy(app)


class Park(db.Model):
    """
    The Park class builds the database of theme parks.
    A Park has attributes of:
        > id number         (integer - this is for internal use in database and numbering table rows in models)
        > category          (string - the current category, which is the same as the html page name. Here: "parks")
        > code              (string - the div id in html that can be called to bring the user to a certain position on the page)
        > name              (string - the name of the park, e.g. "Six Flags Fiesta Texas")
        > location          (string - the city and state/country where the park is located)
        > city              (string - the city where the park is located)
        > state             (string - the state, or country if not USA, where the park is located)
        > size              (string - area of the park grounds in acres; unit included in format, e.g. "500 acres")
        > opened            (string - year the park was first opened)
        > age               (string - current age of the park in years, from its opening year to 2018)
        > season            (string - times of year the park is open)
        > annual visitors   (string - count or estimate, in millions, of the total number of visitors to the park in 2017)
        > ticket price      (string - cost or range of costs, in dollars, of an average day ticket to the park)
        > has fw            (string - "yes" or "no", whether this park contains a Ferris wheel)
        > has rc            (string - "yes" or "no", whether this park contains a roller coaster)
        > has sr            (string - "yes" or "no", whether this park contains a swing ride)
        > had fatal         (string - whether or not any of the accidents we have data on for this park were fatal; "yes" or "no")
        > description       (string - a couple sentences about the park and its contents)
    """
    __tablename__ = 'parks'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String())
    code = db.Column(db.String())
    name = db.Column(db.String())
    location = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    size = db.Column(db.String())
    opened = db.Column(db.String())
    age = db.Column(db.String())
    season = db.Column(db.String())
    annual_visitors = db.Column(db.String())
    ticket_price = db.Column(db.String())
    has_fw = db.Column(db.String())
    has_rc = db.Column(db.String())
    has_sr = db.Column(db.String())
    had_fatal = db.Column(db.String())
    description = db.Column(db.String())

    def __init__(self,id,category,code,name,location,city,state,size,opened,age,season,annual_visitors,ticket_price,has_fw,has_rc,has_sr,had_fatal,description):
        self.id = id
        self.category = category
        self.code = code
        self.name = name
        self.location = location
        self.city = city
        self.state = state
        self.size = size
        self.opened = opened
        self.age = age
        self.season = season
        self.annual_visitors = annual_visitors
        self.ticket_price = ticket_price
        self.has_fw = has_fw
        self.has_rc = has_rc
        self.has_sr = has_sr
        self.had_fatal = had_fatal
        self.description = description


class Ride(db.Model):
    """
    The Ride class builds the database of theme park rides.
    A Ride has attributes of:
        > id number     (integer - this is for internal use in database and numbering table rows in models)
        > category      (string - the current category, which is the same as the html page name. Here: "rides")
        > code          (string - the div id in html that can be called to bring the user to a certain position on the page)
        > name          (string - what the type of ride is called)
        > max height    (string - height of the tallest instance of the ride in the world, in format "550 ft")
        > speed         (string - a description of slow, medium, or fast, relative to other rides)
        > duration      (string - a range of time that a single trip on this ride typically lasts, in minutes)
        > featured in   (string - one of the parks we have data on that includes this ride)
        > had fatal     (string - whether or not any of the accidents we have data on for this type of ride were fatal; "yes" or "no")
        > first built   (string - year that this type of ride was first constructed)
        > history       (string - a description of how and where the ride came about)
        > description   (string - describes ride's structure, motion, and how it carries riders)
    """
    __tablename__ = 'rides'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String())
    code = db.Column(db.String())
    name = db.Column(db.String())
    max_height = db.Column(db.String())
    speed = db.Column(db.String())
    duration = db.Column(db.String())
    featured_in = db.Column(db.String())
    had_fatal = db.Column(db.String())
    first_built = db.Column(db.String())
    history = db.Column(db.String())
    description = db.Column(db.String())

    def __init__(self,id,category,code,name,max_height,speed,duration,featured_in,had_fatal,first_built,history,description):
        self.id = id
        self.category = category
        self.code = code
        self.name = name
        self.max_height = max_height
        self.speed = speed
        self.duration = duration
        self.featured_in = featured_in
        self.had_fatal = had_fatal
        self.first_built = first_built
        self.history = history
        self.description = description


class Accident(db.Model):
    """
    The Accident class builds the database of theme park ride accidents.
    An Accident has attributes of:
        > id number         (integer - this is for internal use in database and numbering table rows in models)
        > category          (string - the current category, which is the same as the html page name. Here: "accidents")
        > code              (string - the div id in html that can be called to bring the user to a certain position on the page)
        > external link     (string - web address of outside source containing information about the accident)
        > on_ride           (string - type of ride the accident occurred on)
        > location          (string - the park, city, and state/country where the accident occurred)
        > park              (string - the park where the accident occurred)
        > city              (string - the city where the accident occurred)
        > state             (string - the state, or country if not USA, where the accident occurred)
        > cause             (string - what kind of error caused the accident, e.g. "Mechanical")
        > damage            (string - the type of damage that was inflicted or the way people were injured, e.g. "Fall")
        > hurt              (string - how many people were affected in the accident, whether rescued without injury, injured, or killed)
        > fatal             (string - whether the accident had at least 1 fatality; "yes" or "no")
        > year              (string - year in which the accident occurred)
        > date of event     (string - specific date on which the accident occurred)
        > description       (string - a summary of what happened in the accident and how people were affected)
    """
    __tablename__ = 'accidents'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String())
    code = db.Column(db.String())
    ext_link = db.Column(db.String())
    on_ride = db.Column(db.String())
    location = db.Column(db.String())
    park = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    cause = db.Column(db.String())
    dmg = db.Column(db.String())
    hurt = db.Column(db.String())
    fatal = db.Column(db.String())
    year = db.Column(db.String())
    date_of_event = db.Column(db.String())
    description = db.Column(db.String())

    def __init__(self,id,category,code,ext_link,on_ride,location,park,city,state,cause,dmg,hurt,fatal,year,date_of_event,description):
        self.id = id
        self.category = category
        self.code = code
        self.ext_link = ext_link
        self.on_ride = on_ride
        self.location = location
        self.park = park
        self.city = city
        self.state = state
        self.cause = cause
        self.dmg = dmg
        self.hurt = hurt
        self.fatal = fatal
        self.year = year
        self.date_of_event = date_of_event
        self.description = description
db.drop_all()
db.create_all()
